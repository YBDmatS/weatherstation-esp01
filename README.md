# [[ WeatherStation - ESP01 ]]



## Description

Developed for a Weather Station student project.

Code C++ (Arduino) for the ESP01 microcontroller to get temperature's report and humidity's report and record the report via the  API in database.

![Schema](Schema.png)

To see the whole project, have a look to the 2 other repositories :

https://gitlab.com/YBDmatS/weatherstation-site

https://gitlab.com/YBDmatS/weatherstation-api


## Material

ESP01 - microcontroller
GY-21 HTU21 - temperature and humidity probe
I2C SSD1306 - LCD screen
CP2102 - USB adapter for ESP01
Raspberry Pi Zero WH - server 
Breadboard and jumper


## Visual

![CablageLCD](cablageLCD.jpg)

