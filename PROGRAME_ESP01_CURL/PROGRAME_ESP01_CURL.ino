#include <ESP8266HTTPClient.h>
#include <Adafruit_HTU21DF.h>
#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ArduinoJson.h>


const char* ssid     = "AndroidAP5627";      // SSID of local network
const char* password = "0ea3c3e1890d";    // Password on network
const char* fullURL = "http://192.168.43.176/API/nouveaureleve";

// set the LCD address to 0x27 for a 16 chars and 2 line display
LiquidCrystal_I2C lcd(0x27,16,2);

// Set sensor
Adafruit_HTU21DF htu = Adafruit_HTU21DF();


// JSON ENCODE
DynamicJsonDocument jsonEncode(float temp, float hum){

    DynamicJsonDocument releve(2048);
    releve["Temperature"] = temp;
    releve["Humidity"] = hum;
    releve["Sonde"]= WiFi.macAddress();
    return releve;
}

void setup() {

  // Activate LCD
  Wire.begin(0,2);
  lcd.init();
  lcd.display();  
  lcd.clear();         
  lcd.backlight(); 

  //Set cursor to character 2 on line 0
  lcd.setCursor(2,0);   
  lcd.print("Welcome!");
  delay(5000);

  // Activate WIFI AND SERVER
  lcd.clear();
  lcd.setCursor(0,0); 
  lcd.print("WIFI connection");
  WiFi.begin(ssid, password);
  lcd.setCursor(0,1); 
  while (WiFi.status() != WL_CONNECTED) {
    lcd.print(".");
    delay(1000);
  }
  

  lcd.clear();
  lcd.setCursor(2,0);
  lcd.print("MY Local IP: "); 
  lcd.setCursor(1,1);
  lcd.print(WiFi.localIP());
  delay(5000);

  // Set Sensor
  Serial.begin(9600);
  if (!htu.begin()) {
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print("Sensor don't find");
    while (1);
  }
  delay(5000);

  // Waiting during data analyse
  lcd.clear();
  lcd.setCursor(5,0);
  lcd.print("Loading...");
  lcd.setCursor(0,1);    


}

void loop() {
  
      float sumTemp = 0;
      float sumHum = 0;
      float avgTemp = 0;
      float avgHum = 0;

    for (int i=0; i<5; i++) {
      float temp = htu.readTemperature();
      float hum = htu.readHumidity();
      sumTemp += temp;
      sumHum += hum;
      delay(1000);
    }

    avgTemp = sumTemp / 5.0f;
    avgHum = sumHum /5.0f;
        
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print("Avg Temp.: "); lcd.print(avgTemp); lcd.print(" C");
    lcd.setCursor(0,1);
    lcd.print("Avg Hum.: "); lcd.print(avgHum); lcd.print(" \%");

    DynamicJsonDocument releveToPost = jsonEncode(avgTemp, avgHum);
    String jsonInStringFormat;
    serializeJson(releveToPost, jsonInStringFormat);
    
    WiFiClient client;
    HTTPClient http;
    http.begin(client, fullURL);    
    http.addHeader("Content-Type", "application/json");
    int httpResponseCode = http.POST(jsonInStringFormat);
    http.end();

}
